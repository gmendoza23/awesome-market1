class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :dni
      t.string :name
      t.string :lastname
      t.string :phone-number
      t.string :birth_date
      t.string :password
    end
  end
end
