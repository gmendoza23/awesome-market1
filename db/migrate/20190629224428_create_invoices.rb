class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.date :release_date
      t.float :tax
      t.string :total
    end
  end
end
