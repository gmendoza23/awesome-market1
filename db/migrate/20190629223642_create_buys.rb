class CreateBuy < ActiveRecord::Migration[5.2]
  def change
    create_table :buys do |t|
      t.date :release_date
      t.string :total
    end
  end
end
