class CreateCorridors < ActiveRecord::Migration[5.2]
  def change
    create_table :corridors do |t|
      t.string :name
      t.integer :number
    end
  end
end
